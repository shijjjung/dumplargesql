"""
    將大量資料庫 load_employees.sql
    拆解成 SSMS 單次讀取上限1000行 sql格式 insertCmdXXX.sql
    並將每份sql檔案執行更新 SQL Server 資料
"""
import sys
import os
import sql_adapter
sql = sql_adapter.sql_adapter()
abs_dir_path = os.getcwd() + "/"
outputPath = abs_dir_path + "insertCmd{0}.sql"
# param 開啟檔案路徑 xxx.sql
# sys.argv[0] filename 
MAX_PER_FILE_LINES = 1000

def str_modfied(str, isEnd):
    if not isEnd and ";" in str:
        return str[::-1].replace(";",",",1)[::-1]
    elif isEnd and str[-1] == ",":
        return str[::-1].replace(",",";",1)[::-1]
    return str
if __name__ == '__main__':
    filepath = sys.argv[1]
    index = 1
    isfill = False
    fileLimit = MAX_PER_FILE_LINES
    try:
        # Using readlines()
        file1 = open(abs_dir_path + filepath, 'r')
        Lines = file1.readlines()   
        fsave = open(outputPath.format(str(index)), "w")
        count = 0      
        # Strips the newline character
        for line in Lines:
            if isfill:
                fsave.close()
                sql.cmdSqlFile(outputPath.format(str(index)))
                index+=1
                isfill = False
                fileLimit = MAX_PER_FILE_LINES     
                newfilename = outputPath.format(str(index))                
                fsave = open(newfilename, "w")          
            count += 1
            isfill = (fileLimit == 1)
            if "VALUES" in line.strip() and "INSERT INTO" in line.strip():
                _str = line.strip().split("VALUES", 1)
                inserCommand = _str[0].replace("`",'') + "VALUES "
                data = _str[1]
                if fileLimit == MAX_PER_FILE_LINES:
                    fsave.write(str_modfied(inserCommand + data, False))
                else:
                    fsave.write(str_modfied(data, isfill | (line == Lines[-1])))
            else:
                if fileLimit == MAX_PER_FILE_LINES:                    
                    fsave.write(str_modfied(inserCommand + line.strip(), False))
                else:
                    fsave.write(str_modfied(line.strip(), isfill | (line == Lines[-1])))
            fileLimit-=1
        fsave.close()
        sql.cmdSqlFile(outputPath.format(str(index)))               
    except IOError as exc:
        raise RuntimeError('Failed to open database') from exc    
        print(os.popen("dir").read())